#ifndef _PINS_H_
#define _PINS_H_

#include "mbed.h"
#include "PpmIn.h"

extern const PinName pin_serial_pc_tx;
extern const PinName pin_serial_pc_rx;

extern const PinName pin_uart_tx;
extern const PinName pin_uart_rx;

extern const PinName pin_ppm_in;
extern const PinName pin_enable;
extern const PinName pin_tach;
extern const PinName pin_brake;
extern const PinName pin_dir;

extern const PinName pin_spi_mosi;
extern const PinName pin_spi_miso;
extern const PinName pin_spi_sck;
extern const PinName pin_spi_cs;

extern const PinName pin_led1;
extern const PinName pin_led2;
extern const PinName pin_led3;

extern const PinName pin_pwm_vel;
extern const PinName pin_pwm_dir;
extern const PinName pin_pwm_init;

extern Timer timer;   // !! max ~30 [min] <- 32-bit [us] counter

extern Serial pc;

#endif // _PINS_H_
