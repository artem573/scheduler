/*=============================================
HoTT stands for Hopping Telemetry Transmission
=============================================*/

#ifndef _HOTT_H_
#define _HOTT_H_

#include "mbed.h"
#include "hott_msg.h"
#include "pins.h"
#include <map>

struct Buffer
{
  static const uint8_t rx_buffer_size = 255;
  static const uint8_t tx_buffer_size = 255;
  volatile uint8_t rx_buffer[rx_buffer_size+1];
  volatile uint8_t tx_buffer[tx_buffer_size+1];
  volatile uint8_t rx_in;
  volatile uint8_t rx_out;
  volatile uint8_t tx_in;
  volatile uint8_t tx_out;
};


class Hott
{
  RawSerial uart; // tx, rx

  static const int rx_schedule = 4000;
  static const int tx_delay_us = 3000;

  static const int text_mode_request_id = 0x7f;
  static const int binary_mode_request_id = 0x80;

  static const int gps_sensor_id = 0x8a;
  static const int eam_sensor_id = 0x8e;

  // Textmode address to simulate (GPS)
  static const int sim_textmode_address = gps_sensor_id;
  
  TextmodeMsg txt_msg;
  EamMsg eam_msg;

  volatile uint32_t tnow;

  Buffer buffer;

  bool uart_is_sending;
  uint32_t serial_timer;
  uint32_t last_check_request;
  bool looking_for_request;
  uint8_t *msg_ptr;
  uint8_t msg_len;
  uint8_t sending_msg_id;
  uint8_t msg_crc;

  void manage_received_data();
  uint8_t bytes_in_rx_buffer();
  uint8_t read_byte_from_rx_buffer();
  void send_byte_through_uart(uint8_t data_to_send);
  void clear_rx_buffer();
  void clear_text_screen();
  void prepare_msg(uint8_t *buffer, uint8_t len);
  void send_message_through_uart();
  void init_telemetry();

  void Rx_interrupt();
  void Tx_interrupt();
  void enable_transmitter();  
  void enable_receiver(); 
  void disable_transmitter();  
  void disable_receiver(); 

 public:
  static const int txt_sensor_id = sim_textmode_address & 0x0f;

  Hott(PinName uart_tx, PinName uart_rx);
  
  void manage_telemetry();

  static void print_word(TextmodeMsg &txt_msg, uint8_t pos, const char *w, bool inverted);

  TextmodeMsg& get_txt_msg();
  EamMsg& get_eam_msg();
};

#endif // _HOTT_H_
