#ifndef _PPMIN_H_
#define _PPMIN_H_

#include "mbed.h"
#include <vector>

class PpmIn
{
  InterruptIn p;
  Timer t;

  // Time in seconds
  double period, pulsewidth;
  
  uint8_t channel;
  bool eval_signals;

  void count_on_rise();

 public:
  const int ppm_size;

  // Time in seconds
  const double max_period;
    
  std::vector<double> channels;

  PpmIn(PinName pin, int ppm_size, double max_period);

  void disable_interrupt();
  void enable_interrupt();
};

#endif // _PPMIN_H_
