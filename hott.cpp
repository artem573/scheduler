#include "hott.h"

Hott::Hott(PinName uart_tx, PinName uart_rx):
  uart(uart_tx, uart_rx),
  tnow(0),
  uart_is_sending(false),
  last_check_request(0),
  looking_for_request(true),
  msg_ptr(0),
  msg_len(0),
  sending_msg_id(0),
  msg_crc(0)
{
  init_telemetry();
}

void Hott::init_telemetry()
{
  // Setup hott port
  uart.baud(19200);

  // Init text msg
  memset(&txt_msg, 0, sizeof(txt_msg));
  txt_msg.start_byte = 0x7b;
  txt_msg.txt_sensor_id = txt_sensor_id;
  txt_msg.warning_beeps = 0x00;
  txt_msg.stop_byte = 0x7d;

  // Init EAM msg
  memset(&eam_msg, 0, sizeof(eam_msg));
  eam_msg.start_byte = 0x7c;
  eam_msg.eam_sensor_id = eam_sensor_id;
  eam_msg.sensor_id = 0xe0;
  eam_msg.stop_byte = 0x7d;
}

// Have to be called periodically (≈1ms) by timer scheduler
void Hott::manage_telemetry()
{
  tnow = timer.read_us();
    
  // If not sending messages process data received
  if(!uart_is_sending)
    {
      manage_received_data();
    }

  // No data to send
  if(msg_ptr == 0)
    {
      return;
    }

  if(uart_is_sending && (tnow - serial_timer < tx_delay_us))
    {
      return;
    }
  
  send_message_through_uart();
  serial_timer = tnow;
}

void Hott::manage_received_data()
{
  enable_receiver();
  uint8_t bytes_received = bytes_in_rx_buffer();

  // Wait for proper request (have to be 2 bytes)
  if(bytes_received <= 1)
    {
      return;
    }

  // A train [request timing] is gone, should wait for proper request (have to be 2 bytes)
  if(bytes_received > 2)
    {
      clear_rx_buffer();
      looking_for_request = true;
      return;
    }

  // If 2 bytes received should wait some time before sending message back to console
  if(looking_for_request)
    {
      last_check_request = tnow;
      looking_for_request = false;
      return;
    }

  bool enough_time_passed = (tnow - last_check_request) >= rx_schedule;
  if(!enough_time_passed)
    {
      return;
    }

  looking_for_request = true;

  // We have a valid request, check for address
  uint8_t request_mode_id = read_byte_from_rx_buffer();
  uint8_t request_sensor_id = read_byte_from_rx_buffer();

  switch(request_mode_id)
    {
    case text_mode_request_id:
      txt_msg.start_byte = 0x7b;
      txt_msg.stop_byte = 0x7d;
      if((request_sensor_id >> 4) == txt_sensor_id)
        {
          prepare_msg((uint8_t *)&txt_msg, sizeof(txt_msg));
        }
      break;
            
    case binary_mode_request_id:
      if(request_sensor_id == eam_sensor_id)
        {
          prepare_msg((uint8_t *)&eam_msg, sizeof(eam_msg));
        }
      break;
      
    default:
      break;
    }
}

void Hott::prepare_msg(uint8_t *buffer, uint8_t len)
{
  if(!uart_is_sending)
    {
      msg_ptr = buffer;
      msg_len = len + (sizeof(msg_crc)); // len + 1 byte for crc
      sending_msg_id = buffer[1];        // HoTT msg id is the second byte
    }
}

void Hott::send_message_through_uart()
{
  // New message to send - should preparing for sending new message
  if(!uart_is_sending)
    {
      uart_is_sending = true;
      // enable_transmitter();  // Switch to transmit mode
      msg_crc = 0;
      return;
    }

  // All data sent - clear all stuff and start receiving
  if(msg_len == 0)
    {
      msg_ptr = NULL;
      uart_is_sending = false;
      sending_msg_id = 0; // Clear current hott msg id
      // disable_transmitter();
      enable_receiver();
      clear_rx_buffer();
      return;
    }
  
  --msg_len;
  if(msg_len == 0)
    {
      send_byte_through_uart(msg_crc++);
      return;
    }

  msg_crc += *msg_ptr;
  send_byte_through_uart(*msg_ptr++);
}

void Hott::send_byte_through_uart(uint8_t data_to_send)
{
  if (uart.writeable())
    {
      uart.putc(data_to_send);
    }
}

uint8_t Hott::bytes_in_rx_buffer()
{
  return (buffer.rx_in >= buffer.rx_out ?
          buffer.rx_in - buffer.rx_out :
          buffer.rx_in + buffer.rx_buffer_size - buffer.rx_out);
}

// Read a bit from the large rx buffer from rx interrupt routine
uint8_t Hott::read_byte_from_rx_buffer()
{
  uint8_t temp = buffer.rx_buffer[buffer.rx_out];
  buffer.rx_out = (buffer.rx_out + 1) % buffer.rx_buffer_size;
  return temp;
}

void Hott::clear_rx_buffer()
{
  buffer.rx_out = buffer.rx_in;
}

// Printing to Textframe
void Hott::print_word(TextmodeMsg &txt_msg, uint8_t pos, const char *w, bool inverted)
{
  for (uint8_t index = 0; ; index++)
    {
      if (w[index] == 0x0)
        {
          break;
        }
      else
        {
          txt_msg.msg_txt[index+pos] = inverted ? w[index] +128: w[index];
        }
    }
}

void Hott::enable_transmitter()
{
  uart.attach(this, &Hott::Tx_interrupt, RawSerial::TxIrq);
}

void Hott::disable_transmitter()
{
  uart.attach(NULL, RawSerial::TxIrq);
}

// Switch to receive mode
void Hott::enable_receiver()
{
  uart.attach(this, &Hott::Rx_interrupt, RawSerial::RxIrq);
}

void Hott::disable_receiver()
{
  uart.attach(NULL, RawSerial::RxIrq);
}

// Interrupt Routine to read in data from serial port
void Hott::Rx_interrupt()
{
  // Loop just in case more than one character is in UART's receive FIFO buffer
  // Stop if buffer full
  while (uart.readable())
    {
      buffer.rx_buffer[buffer.rx_in] = uart.getc();
      buffer.rx_in = (buffer.rx_in + 1) % buffer.rx_buffer_size;
    }

  disable_receiver();
}

// Interupt Routine to write out data to serial port
void Hott::Tx_interrupt()
{
  // Loop to fill more than one character in UART's transmit FIFO buffer
  // Stop if buffer empty
  while ((uart.writeable()) && (buffer.tx_in != buffer.tx_out))
    {
      uart.putc(buffer.tx_buffer[buffer.tx_out]);
      buffer.tx_out = (buffer.tx_out + 1) % buffer.tx_buffer_size;
    }

  disable_transmitter();
}

TextmodeMsg& Hott::get_txt_msg()
{
  return txt_msg;
}

EamMsg& Hott::get_eam_msg()
{
  return eam_msg;
}
