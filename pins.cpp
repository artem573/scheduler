#include "pins.h"

const PinName pin_serial_pc_tx = USBTX;
const PinName pin_serial_pc_rx = USBRX;

const PinName pin_uart_tx = PC_10;
const PinName pin_uart_rx = PC_11;

const PinName pin_ppm_in = D8; //D4;
const PinName pin_enable = D4;

const PinName pin_led1 = PA_6;
const PinName pin_led2 = PA_7;
const PinName pin_led3 = LED1;

const PinName pin_tach = D7;
const PinName pin_brake = D9;
const PinName pin_dir = D2; //D5;

const PinName pin_spi_mosi = SPI_MOSI;
const PinName pin_spi_miso = SPI_MISO;
const PinName pin_spi_sck = SPI_SCK;
const PinName pin_spi_cs = SPI_CS;


const PinName pin_pwm_vel = D5;
const PinName pin_pwm_dir = D6;
const PinName pin_pwm_init = D3;


Serial pc(pin_serial_pc_tx, pin_serial_pc_rx);


Timer timer;
