#include "PpmIn.h"

PpmIn::PpmIn(PinName pin, int ppm_size, double max_period):
  p(pin),
  ppm_size(ppm_size),
  max_period(max_period)
{
  channels.resize(ppm_size);
  
  p.rise(NULL);
  p.fall(NULL);
  period = 0.0;
  t.start();
  channel = 0;
  eval_signals = false;
}

void PpmIn::count_on_rise()
{
  period = t.read();

  if(period > max_period)
    {
      channel = 0;
      eval_signals = true;
    }
  else if(eval_signals)
    {
      channels[channel] = period;
      channel++;
      if(channel >= ppm_size)
        {
          disable_interrupt();
          eval_signals = false;
        }
    }

  t.reset();
}

void PpmIn::disable_interrupt()
{
  t.stop();
  p.rise(NULL);
}

void PpmIn::enable_interrupt()
{
  t.start();
  // TODO: check if flag "enabled" is nessesary: "if(!enabled)..."
  p.rise(this, &PpmIn::count_on_rise);
}
