#ifndef MOTOR_H_
#define MOTOR_H_

#include <functional>

#include "mbed.h"
#include "drv8308.h"

struct Motor
{
  DRV8308 drv;
  InterruptIn tach;
  DigitalOut brake;
  DigitalOut dir;
  DigitalOut enable;

  int rot;
  void on_rotation()
  {
    if (dir)
      rot += 1;
    else
      rot -= 1;
  };

Motor(PinName mosi, PinName miso, PinName sck, PinName cs, PinName tach_term, PinName brake_term, PinName dir_term, PinName en_term)
: drv(mosi, miso, sck, cs), tach(tach_term), brake(brake_term), dir(dir_term), enable(en_term,0), rot(0)
  {
    tach.rise(this, &Motor::on_rotation);
  };

  void set_brake(bool brake) {this->brake = brake;};
  void set_dir(bool dir) {this->dir = dir;};
  void set_speed(double speed)
  {
    drv.reg0B.speed = speed*1100; //2047; //4095;
    drv.write_register(drv.reg0B);
  };

  unsigned int get_speed()
  {
    DRV8308::Reg0B temp;
    drv.read_register(temp);
    return temp.speed;
  }
  
  DRV8308::Reg2A get_status()
  {
    DRV8308::Reg2A temp;
    drv.read_register(temp);
    drv.reg2A.value = 0;
    drv.write_register(drv.reg2A);
    return temp;
    
    /*
    drv.reg2A.value = 0;
    drv.write_register(drv.reg2A);
    drv.read_register(drv.reg2A);
    return drv.reg2A;
    */
  };


  void init()
  {
    if(!enable) enable = 1;
    drv.reg00.ag_setpt = 0b0101;
    drv.reg00.spdmode = 0b10;
    drv.reg00.retry = 1;
    drv.reg00.fgsel = 0b01;
    drv.write_register(drv.reg00);

    drv.reg02.spdrevs = 10;
    drv.write_register(drv.reg02);

    drv.reg0A.loopgain = 200;
    drv.reg0A.ocpth = 0b01;
    drv.write_register(drv.reg0A);

    drv.reg03.mod120 = 3970;
    drv.write_register(drv.reg03);

    drv.reg04.autogain = 1;
    drv.reg04.autoadv = 1;
    drv.reg04.lrtime = 0b10; //0b00;
    drv.reg04.idrive = 0b100;
    drv.write_register(drv.reg04);

    drv.reg06.bypflt = 1;
    drv.write_register(drv.reg06);

    drv.reg08.bypcomp = 1;
    drv.write_register(drv.reg08);
  }
};

#endif
