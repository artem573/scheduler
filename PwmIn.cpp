#include "PwmIn.h"

PwmIn::PwmIn(PinName p_in) : p(p_in)
{
  p.rise(NULL);
  p.fall(NULL);
  period = 0.0;
  pulsewidth = 0.0;
  t.start();
  counter = 0;
}

void PwmIn::rise()
{
  counter++;
  period = t.read();
  t.reset();
  if(counter >= 2)
    {
      disable_interrupt();
    }
}

void PwmIn::fall()
{
  pulsewidth = t.read();
}

void PwmIn::disable_interrupt()
{
  t.stop();
  p.rise(NULL);
  p.fall(NULL);
}

void PwmIn::enable_interrupt()
{
  t.start();
  counter = 0;
  // TODO: check if flag "enabled" is nessesary: "if(!enabled)..."
  p.rise(this, &PwmIn::rise);
  p.fall(this, &PwmIn::fall);
}
