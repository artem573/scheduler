#include "scheduler.h"

TaskType Scheduler::null_task = { 0, 0, 0, [](void){}, RunFlag::Disabled, RunFreq::Never };

Scheduler::Scheduler():
  sort_flag(false)
{
  // Do nothing
}


void Scheduler::run_tasks()
{
  auto cur_tasks = tasks;
  tasks.clear();

  //  int task_index = 0;
  for(auto & it_t: cur_tasks)
    {
      if(it_t.last_tick + it_t.interval < timer.read_ms())
        {
          it_t.func();
          it_t.last_tick = timer.read_ms();
          
          if(it_t.run_freq != RunFreq::Once)
            {
              tasks.push_back(it_t);
            }
        }
      else
        {
          tasks.push_back(it_t);
        }
    }

  if(sort_flag)
    {
      std::sort(tasks.begin(), tasks.end(), compare_tasks);
      sort_flag = false;
    }
}

bool TaskType::operator<(const TaskType& another_task)
{
  return static_priority < another_task.static_priority;
}

bool Scheduler::compare_tasks(const TaskType& task1, const TaskType& task2)
{
  return task1.static_priority < task2.static_priority;
}

void Scheduler::add_task(uint32_t interval, uint8_t static_priority, std::function<void()> func)
{
  tasks.push_back({ interval, 0, static_priority, func, RunFlag::Disabled, RunFreq::Always });

  sort_flag = true;
}

void Scheduler::add_task_run_once(uint8_t static_priority, std::function<void()> func)
{
  tasks.push_back({ 0, 0, static_priority, func, RunFlag::Disabled, RunFreq::Once });

  sort_flag = true;
}
