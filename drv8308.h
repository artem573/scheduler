#ifndef DRV8308_H
#define DRV8308_H

#include "mbed.h"

struct DRV8308
{
  SPI spi;
  DigitalOut cs;

  struct Reg00 {static const unsigned char addr = 0x00;
    union {uint16_t value = 0; struct {unsigned int retry:1, brkmod:1, fgsel:2, spdmode:2, pwmf:2, synrect:1, brkpol:1, dirpol:1, enpol:1, ag_setpt:4;};};};
  struct Reg01 {static const unsigned char addr = 0x01;
    union {uint16_t value = 0; struct {unsigned int advance:8, rsvd:8;};};};
  struct Reg02 {static const unsigned char addr = 0x02;
    union {uint16_t value = 0; struct {unsigned int minspd:8, spdrevs:8;};};};
  struct Reg03 {static const unsigned char addr = 0x03;
    union {uint16_t value = 0; struct {unsigned int mod120:12, speedth:3, basic:1;};};};
  struct Reg04 {static const unsigned char addr = 0x04;
    union {uint16_t value = 0; struct {unsigned int idrive:3, dtime:3, tdrive:2, ensine:1, autogain:1, autoadv:1, delay:1, hallrst:2, lrtime:2;};};};
  struct Reg05 {static const unsigned char addr = 0x05;
    union {uint16_t value = 0; struct {unsigned int spdgain:12, intclk:3, rsvd:1;};};};
  struct Reg06 {static const unsigned char addr = 0x06;
    union {uint16_t value = 0; struct {unsigned int filk1:12, bypflt:1, rsvd:2, hallpol:1;};};};
  struct Reg07 {static const unsigned char addr = 0x07;
    union {uint16_t value = 0; struct {unsigned int filk2:12, rsvd:4;};};};
  struct Reg08 {static const unsigned char addr = 0x08;
    union {uint16_t value = 0; struct {unsigned int comk1:12, bypcomp:1, rsvd:3;};};};
  struct Reg09 {static const unsigned char addr = 0x09;
    union {uint16_t value = 0; struct {unsigned int comk2:12, aa_setpt:4;};};};
  struct Reg0A {static const unsigned char addr = 0x0A;
    union {uint16_t value = 0; struct {unsigned int loopgain:10, vreg_en:1, ovth:1, ocpth:2, ocpdeg:2;};};};
  struct Reg0B {static const unsigned char addr = 0x0B;
    union {uint16_t value = 0; struct {unsigned int speed:12, rsvd:4;};};};
  struct Reg2A {static const unsigned char addr = 0x2A;
    union {uint16_t value = 0; struct {unsigned int ocp:1, cpoc:1, ots:1, uvlo:1, cpfail:1, vmov:1, rlock:1, rsvd:10;};};};

  Reg00 reg00;
  Reg01 reg01;
  Reg02 reg02;
  Reg03 reg03;
  Reg04 reg04;
  Reg05 reg05;
  Reg06 reg06;
  Reg07 reg07;
  Reg08 reg08;
  Reg09 reg09;
  Reg0A reg0A;
  Reg0B reg0B;
  Reg2A reg2A;

  DRV8308(PinName in, PinName out, PinName clk, PinName cs)
    : spi(in, out, clk), cs(cs)
  {
    this->cs = 0;
    spi.format(8, 3);
    spi.frequency(100000);
  }

  template<class T>
  void write_register(T reg)
  {
    cs = 1;
    spi.write(reg.addr);
    wait(0.000001);
    int dat = reg.value;
    spi.write((dat >> 8) & 0xFF);
    wait(0.000001);
    spi.write(dat & 0xFF);
    cs = 0;
  }

  template<class T>
  T read_register(T& reg)
  {
    cs = 1;
    wait(0.000001);
    spi.write(reg.addr | 1<<7);
    wait(0.000001);
    int dat = spi.write(0x00);
    wait(0.000001);
    dat = spi.write(0x00) | dat<<8;
    wait(0.000001);
    cs = 0;
    reg.value = dat;
    return reg;
  }
};


#endif
