#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include "mbed.h"
#include "PpmIn.h"
#include "PwmIn.h"
#include "hott.h"
#include "scheduler.h"

enum class State {Arm, Unarmed, Idle, Moving, Braking, ChangingDir};
enum class Signal {MotorReinit, Move, Stop, NearLBP, NearBP, InvDir, NoSignal, Timer, GoHome, LowBat, ChangeVelocity};

struct PpmData
{
  double velocity = 0;
  bool dir = true;
  bool init = false;
  bool brake = false;
};

struct DisplayInfo
{
  double velocity = 0;
  int rot = 0;
  bool armed = false;
  bool braking = false;
  bool dir = true;
  State status = State::Idle;
};

class Console
{
  // Rethink this approach
  const double ppm_max = 0.0019;
  const double ppm_min = 0.0011;
  const double ppm_tol = 0.001;
  const double ppm_mid = 0.5*(ppm_min + ppm_max);
  const double small_vel = 0.01;

  enum PpmSignal { Velocity = 2, Dir = 5, Brake = 4, Init = 6 };

  Scheduler& scheduler;

  PpmIn ppm;
  PwmIn pwm_vel;
  PwmIn pwm_dir;
  PwmIn pwm_init;

  Hott hott;

  void (*dispatch)(Signal signal);

  PpmData ppm_data;
  DisplayInfo display_info;

  void update_ppm();
  void manage_telemetry();
  void update_txt_msg();
  void update_eam_msg();

 public:
  Console(Scheduler &s, PinName ppm_in, PinName pwm_vel_pin, PinName pwm_dir_pin, PinName pwm_init_pin, PinName uart_tx, PinName uart_rx);

  void attach_dispatcher(void (*callback_func)(Signal signal));

  const PpmData& read_ppm();
  DisplayInfo& get_display_info();

};

#endif // _CONSOLE_H_
