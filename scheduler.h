#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include "mbed.h"
#include "PpmIn.h"
#include "pins.h"
#include <algorithm>
#include <vector>
#include <functional>

enum class RunFlag {Enabled, Disabled};
enum class RunFreq {Never, Once, Always};

struct TaskType
{
  // Time in milliseconds
  uint32_t interval;

  // Time in milliseconds
  uint32_t last_tick;

  // The higher a priority the sooner a task execution
  uint8_t static_priority;

  std::function<void()> func;
  volatile RunFlag run_flag;
  RunFreq run_freq;

  bool operator<(const TaskType& another_task);
};

class Scheduler
{
  Ticker ticker; // !! max ~30 [min] <- 32-bit [us] counter

  std::vector<TaskType> tasks;
  std::vector<TaskType*> queue;

  static TaskType null_task;

  bool sort_flag;

 public:
  Scheduler();

  void add_task(uint32_t interval, uint8_t static_priority, std::function<void()> func);
  void add_task_run_once(uint8_t static_priority, std::function<void()> func);
  void run_tasks();
  void delete_unused_tasks();
  static bool compare_tasks(const TaskType& task1, const TaskType& task2);
};



#endif // _SCHEDULER_H_
