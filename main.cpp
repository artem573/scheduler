#include "mbed.h"
#include "pins.h"
#include "scheduler.h"
#include "console.h"
#include "motor.h"

State state = State::Unarmed;

// DigitalOut led1(pin_led1);
// DigitalOut led2(pin_led2);
// DigitalOut led3(pin_led3);



void init();
void blink_led1();
void blink_led2();
void blink_led3();
void print_status();
void send_msg();
void print_yo();
void rotatation();

void callback_func(Signal signal);

Motor motor(pin_spi_mosi, pin_spi_miso, pin_spi_sck, pin_spi_cs, pin_tach, pin_brake, pin_dir, pin_enable);
Scheduler scheduler;
Console console(scheduler, pin_ppm_in, pin_pwm_vel, pin_pwm_dir, pin_pwm_init, pin_uart_tx, pin_uart_rx);

int main()
{
  init();

  while (1)
    {
      scheduler.run_tasks();
    }

  return 0;
}

void init()
{
  timer.start();

  //led1 = led2 = led3 = 1;

  //scheduler.add_task(500,  0, blink_led3);
  scheduler.add_task(1000, 3, print_status);
  scheduler.add_task(4000, 2, send_msg);

  console.attach_dispatcher(callback_func);
}



// void blink_led1()
// {
//   led1 = !led1;
// }

// void blink_led2()
// {
//   led2 = !led2;
// }

// void blink_led3()
// {
//   led3 = !led3;
// }


void print_status()
{
  const PpmData& ppm_data = console.read_ppm();
  pc.printf("speed %f dir %d init %d brake %d rot %d\n\r", ppm_data.velocity, ppm_data.dir, ppm_data.init, ppm_data.brake, motor.rot);

  auto speed = motor.get_speed();
  pc.printf("speed %d \n\r", speed);
    
  auto reg2A = motor.get_status();
  pc.printf("2A: ocp:%d cpoc:%d ots:%d uvlo:%d cpfail:%d vmov:%d rlock:%d - %x\n\r", reg2A.ocp, reg2A.cpoc, reg2A.ots, reg2A.uvlo, reg2A.cpfail, reg2A.vmov, reg2A.rlock, reg2A.value);
}

void send_msg()
{
  const PpmData& ppm_data = console.read_ppm();
  DisplayInfo& display_info = console.get_display_info();
  display_info.velocity = ppm_data.velocity;
  display_info.braking = ppm_data.brake;
  display_info.dir = ppm_data.dir;
  display_info.rot = motor.rot;
}

void print_yo()
{
  pc.printf("\n");
}

void callback_func(Signal signal)
{
  const PpmData& ppm_data = console.read_ppm();
  switch(signal)
    {
    case Signal::MotorReinit :
      motor.init();
      pc.printf("motor reinited \n\r");
      break;

    case Signal::Stop :
      motor.set_brake(ppm_data.brake);
      pc.printf("braking %d\n\r", ppm_data.brake);
      break;

    case Signal::InvDir :
      motor.set_dir(ppm_data.dir);
      pc.printf("dir changed %d\n\r", ppm_data.dir);
      break;

    case Signal::ChangeVelocity :
      {
        motor.set_speed(ppm_data.velocity);
        pc.printf("changing velocity %f\n\r", ppm_data.velocity);
      }
      break;

    default :
      break;
    }
}
