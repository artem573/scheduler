#include "console.h"

Console::Console(Scheduler &s, PinName ppm_in, PinName pwm_vel_pin, PinName pwm_dir_pin, PinName pwm_init_pin, PinName uart_tx, PinName uart_rx):
  scheduler(s),
  ppm(ppm_in,8,0.005),
  pwm_vel(pwm_vel_pin),
  pwm_dir(pwm_dir_pin),
  pwm_init(pwm_init_pin),
  hott(uart_tx, uart_rx),
  dispatch(0)
{
  scheduler.add_task(500, 0, std::bind(&Console::update_txt_msg, this));
  //  scheduler.add_task(2, 0, std::bind(&Console::update_eam_msg, this));
  scheduler.add_task(50, 4, std::bind(&Console::update_ppm, this));
  scheduler.add_task(1, 5, std::bind(&Console::manage_telemetry, this));
}

const PpmData& Console::read_ppm()
{
  return ppm_data;
}

void Console::update_ppm()
{
  bool velocity_change = false;
  // double new_velocity = (ppm.channels[PpmSignal::Velocity] - ppm_min) / (ppm_max - ppm_min);
  double new_velocity = (pwm_vel.pulsewidth - ppm_min) / (ppm_max - ppm_min);
  // TODO: rewrite to min(), max()
  new_velocity = new_velocity < small_vel ? 0 : new_velocity;
  new_velocity = new_velocity > 1 ? 1 : new_velocity;
  if(abs(new_velocity - ppm_data.velocity) > small_vel)
    {
      ppm_data.velocity = new_velocity;
      velocity_change = true;
    }

  bool dir_change = false;
  // bool new_dir = ppm.channels[PpmSignal::Dir] > ppm_mid;
  bool new_dir = pwm_dir.pulsewidth > ppm_mid;

  if(new_dir != ppm_data.dir)
    {
      ppm_data.dir = new_dir;
      dir_change = true;
    }

  bool init = false;
  //  bool init_new = (ppm.channels[PpmSignal::Init] > ppm_mid);
  bool init_new = pwm_init.pulsewidth > ppm_mid;
  if(ppm_data.init != init_new)
    {
      ppm_data.init = init_new;
      init = true;
    }
  
  bool brake_change = false;
  //  bool brake_new = (ppm.channels[PpmSignal::Brake] > ppm_mid);
  bool brake_new = new_velocity < small_vel;
  if (ppm_data.brake != brake_new)
    {
      ppm_data.brake = brake_new;
      brake_change = true;
    }

  // Put callbacks here
  if(dispatch)
    {
      if(init && ppm_data.init) dispatch(Signal::MotorReinit);
      if(velocity_change) dispatch(Signal::ChangeVelocity);
      if(dir_change) dispatch(Signal::InvDir);
      if(brake_change) dispatch(Signal::Stop);
    }

  //  ppm.enable_interrupt();
  pwm_vel.enable_interrupt();
  pwm_dir.enable_interrupt();
  pwm_init.enable_interrupt();
}

void Console::manage_telemetry()
{
  hott.manage_telemetry();
}

void Console::attach_dispatcher(void (*callback_func)(Signal signal))
{
  dispatch = callback_func;
}

void Console::update_eam_msg()
{
  EamMsg &eam_msg = hott.get_eam_msg();

  eam_msg.batt1_voltage_L = 0;
  eam_msg.batt2_voltage_L = 0;
  eam_msg.temp1 = 30;           // (int8_t)((barometer.get_temperature() / 10) + 20);
  eam_msg.temp2 = 20;	         // 0°
  eam_msg.altitude_L = 200*ppm_data.velocity;   // 200;     // (int)((current_loc.alt - home.alt) / 100)+500;
  eam_msg.current_L = 100;      // current_amps1*10;
  eam_msg.main_voltage_L = 40;  // (int)(battery_voltage1 * 10.0);
  eam_msg.batt_cap_L = 60;      // current_total1 / 10;
  eam_msg.speed_L = 0;         // (int)((float)(g_gps->ground_speed * 0.036));

  eam_msg.climbrate_L = 30;     // 30000 + climb_rate;
  eam_msg.climbrate3s = 120;    // 120 + (climb_rate / 100);  // 0 m/3s using filtered data here

  eam_msg.alarm_invers2 &= 0x7f;
}

void Console::update_txt_msg()
{
  TextmodeMsg &txt_msg = hott.get_txt_msg();

  // Clear text screen
  memset(txt_msg.msg_txt, 0x20, sizeof(txt_msg.msg_txt));
  txt_msg.txt_sensor_id = hott.txt_sensor_id;

  const char* logo = "------ RC_PARK ------";
  hott.print_word(txt_msg, (21*1), logo, 0 );

  hott.print_word(txt_msg, (21*2), "0=============>=====1", 0 );

  char res[100];
  char buf[100];
  strcpy(res,"status: ");

  switch(display_info.status)
    {
    case State::Idle:
      strcpy(buf,"Idle");
      break;

    case State::Moving:
      strcpy(buf,"Moving");
      break;

    case State::Braking:
      strcpy(buf,"Braking");
      break;

    case State::ChangingDir:
      strcpy(buf,"ChangingDir");
      break;

    default:
      break;
    };

  strcat(res,buf);
  hott.print_word(txt_msg, (21*4), res, 0 );

  sprintf(res, "V: %4.2f  R: %5d", display_info.velocity, display_info.rot);
  hott.print_word(txt_msg, (21*5), res, 0 );

  const char* armed = display_info.armed ? "       armed!" : "      unarmed!";
  hott.print_word(txt_msg, (21*6), armed, 0);

  hott.print_word(txt_msg, (21*7), logo , 0 );
}

DisplayInfo& Console::get_display_info()
{
  return display_info;
}
