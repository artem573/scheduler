#ifndef _PWMIN_H_
#define _PWMIN_H_

#include "mbed.h"

/** PwmIn class to read PWM inputs
 * 
 * Uses InterruptIn to measure the changes on the input
 * and record the time they occur
 *
 * @note uses InterruptIn, so not available on p19/p20
 */
class PwmIn
{
  InterruptIn p;
  Timer t;
  double period;
  uint8_t counter;

 public:
  PwmIn(PinName p_in);
  double pulsewidth;
  
  void disable_interrupt();
  void enable_interrupt();

  void rise();
  void fall();
};

#endif // _PWMIN_H_
